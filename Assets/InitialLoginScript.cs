﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Lambda;
using Amazon.S3;
using UnityEngine.Networking;
using Amazon.Lambda.Model;
using System.Text;
using UnityEngine.SceneManagement;
using Amazon.S3.Model;
using System.IO;

public class InitialLoginScript : MonoBehaviour {
    string apiLink = "https://b13sssxs43.execute-api.us-east-1.amazonaws.com/PostToDatabase/";
    public InputField username;
    public InputField password;
    public InputField email;
    public new InputField name;
    public Toggle reseller;
    CognitoAWSCredentials credentials;
    public Text errorMessage;
    public void Start()
    {
        UnityInitializer.AttachToGameObject(this.gameObject);
        credentials = new CognitoAWSCredentials(
   "us-east-1:cda77550-d624-4016-ac43-5887e4a8bdf1", // Identity pool ID
   RegionEndpoint.USEast1 // Region
);

    }
    // Initialize the Amazon Cognito credentials provide
    public IEnumerator GetText()
    {
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;
        AmazonLambdaClient client = new AmazonLambdaClient(credentials, RegionEndpoint.USEast1);

        WWWForm data2 = new WWWForm();
        data2.AddField("name", name.text);
        data2.AddField("username", username.text);
        data2.AddField("password", password.text);
        data2.AddField("email", email.text);
        data2.AddField("resellerChoice", reseller.isOn + "");

        WWW form = new WWW(apiLink, data2);
        yield return form;
        string response = form.text;
        Debug.Log(response);
        if (response.Contains("already added")) {
            errorMessage.text = "User has already been added";

        }else if (response.Contains("user added")|| response.Contains("added")) {
            errorMessage.text = "";
            SceneManager.LoadScene("Main");
            
        }




    }

    public void runEnumerator() {
        StartCoroutine(GetText());
    }

    public void useEbay() {
        //username = eBay.GetItem();
    }

}
