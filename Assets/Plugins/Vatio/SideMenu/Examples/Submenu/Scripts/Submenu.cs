﻿using UnityEngine;
using Vatio.UI;

namespace Vatio.Examples
{
    /// <summary>
    /// This script controls the submenus hierarchy.
    /// It's a good example on how to make a view hierarchy for the menu.
    /// Note the "SideMenuStateChanged(SideMenu.MenuState state)" method - it's called from the SideMenu script on it's state change.
    /// It enables us to show the main menu each time the menu is shown regardless of the state it was in when it was hidden.
    /// </summary>
    public class Submenu : MonoBehaviour
    {
        public GameObject[] menus;
        public GameObject topMenu;

        public GameObject cubeParent;
        public GameObject cube;

        public void SideMenuStateChanged(SideMenu.MenuState state)
        {
            // Go to the top menu when the menu is hidden
            if (state == SideMenu.MenuState.Out)
                GoToMenu(topMenu);
        }

        public void HideAllMenus()
        {
            if(menus != null)
            {
                foreach(GameObject menu in menus)
                {
                    menu.SetActive(false);
                }
            }
        }

        public void GoToMenu(GameObject menu)
        {
            HideAllMenus();
            menu.SetActive(true);
        }

        public void AssignColor1()
        {
            cube.GetComponent<MeshRenderer>().material.color = Color.blue;
        }

        public void AssignColor2()
        {
            cube.GetComponent<MeshRenderer>().material.color = Color.cyan;
        }

        public void AssignColor3()
        {
            cube.GetComponent<MeshRenderer>().material.color = Color.magenta;
        }

        public void AssignPosition(float position)
        {
            cubeParent.transform.localPosition = new Vector3(0, position, 0);
        }

        public void AssignRotation(float rotation)
        {
            cubeParent.transform.localRotation = Quaternion.Euler(0, rotation, 0); ;
        }

        public void AssignScale(float scale)
        {
            cubeParent.transform.localScale = new Vector3(scale, scale, scale);
        }
    }
}
