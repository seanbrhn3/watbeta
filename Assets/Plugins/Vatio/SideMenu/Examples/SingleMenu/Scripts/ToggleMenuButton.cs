﻿using UnityEngine;
using Vatio.UI;

namespace Vatio.Examples
{
    /// <summary>
    /// This script toggles the side menu when attached to the toggle button. Feel free to use it in Your projects.
    /// </summary>
    public class ToggleMenuButton : MonoBehaviour
    {
        public SideMenu sideMenu;

        public void ToggleSideMenu()
        {
            sideMenu.Toggle();
        }
    }
}
