﻿using UnityEngine;

namespace Vatio.Examples
{
    /// <summary>
    /// This script shows the appropriate GameObject. It is called when a button is pressed.
    /// </summary>
    public class ObjectsManager : MonoBehaviour
    {
        public GameObject[] objects;
       
        private void HideAllObjects()
        {
            if(objects != null)
            {
                foreach(GameObject go in objects)
                {
                    go.SetActive(false);
                }
            }
        }

        public void ShowObject(GameObject go)
        {
            HideAllObjects();
            go.SetActive(true);
        }
    }
}
