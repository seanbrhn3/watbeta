﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif 
using UnityEngine;
using UnityEngine.UI;

public class DisplaySprite : MonoBehaviour {
    public GameObject displaySprite;

    public Sprite sprite;
    // Use this for initialization
    void Start () {
        #if UNITY_EDITOR
        AssetDatabase.Refresh();
        #endif
        displaySprite = GameObject.Find("Sprite2Crop");
        Texture2D tex;
        int sc = 0;
        if (!Application.isMobilePlatform)
        {
             tex = Resources.Load("pic", typeof(Texture2D)) as Texture2D;
        }
        else
        {
            print("Android");
            byte[] data = File.ReadAllBytes(Application.persistentDataPath + "/pic.png");
            for (int i = 0; i < 5; i++) print(data[i]);
            /**
            string[] files = Directory.GetFiles(Application.persistentDataPath + "/", ".png");
            print(files.Length);
            if(files.Length > 0)
            {
                string pathToFile = files[sc];
                tex = GetScreenshotImage(pathToFile);

            }
            else
            {
                tex = new Texture2D(0, 0);
            }
            **/
            tex = new Texture2D(Screen.width, Screen.height);
            tex.LoadImage(data);


            //tex = Resources.Load("pic", typeof(Texture2D)) as Texture2D;

        }
        sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        //sprite = Sprite.Create(tex, new Rect(0, 0, Screen.width, Screen.height), new Vector2(0.5f, 0.5f));
        SpriteRenderer SR = displaySprite.GetComponent<SpriteRenderer>();
        SR.sprite = sprite;


    }
	Texture2D GetScreenshotImage(string filepath)
    {
        Texture2D texture = null;
        byte[] filebytes;
        if (File.Exists(filepath))
        {
            filebytes = File.ReadAllBytes(filepath);
            texture = new Texture2D(2, 2, TextureFormat.RGB24, false);
            texture.LoadImage(filebytes);
        }
        return texture;
    }
	// Update is called once per frame
	void Update () {
		
	}
}
