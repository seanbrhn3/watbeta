﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using UnityEngine;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.S3.Model;
using Amazon.S3;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using UnityEngine.UI;

public class Googlevision : MonoBehaviour {
    string apilink = "https://pndpc1zj3a.execute-api.us-east-1.amazonaws.com/productionG/";
    string filepath;
    byte[] imagebytes;
    string res;
    public AmazonS3Client Client;
    public GameObject gvlinks;
    public GameObject link1;
    public GameObject link2;

    // Use this for initialization
    void Start () {

        print("GVStart");
        filepath = Application.dataPath + "\\testpicture3.jpg";
        if (!Application.isMobilePlatform)
        {
            filepath = Application.dataPath + "\\testpicture3.jpg";
        }
        else
        {
            //filepath = Application.persistentDataPath + "/testpicture12.jpg";
            filepath = Application.persistentDataPath + "/testpicture12.jpg";
        }

        UnityInitializer.AttachToGameObject(this.gameObject);
        Amazon.AWSConfigs.HttpClient = Amazon.AWSConfigs.HttpClientOption.UnityWebRequest;
        //var credentials = new CognitoAWSCredentials("us-east-1_uTBtftt9k", RegionEndpoint.USEast1);
        CognitoAWSCredentials credentials = new CognitoAWSCredentials("us-east-1:544de752-3991-4297-9331-836b4145d33b", RegionEndpoint.USEast1);
        //var credentials = new CognitoAWSCredentials("us-east-1:72fb36e6-66c8-4e33-8b5e-dd4054c26511", RegionEndpoint.USEast1);
        //var Client = new AmazonLambdaClient(credentials, RegionEndpoint.USEast1);
        Client = new AmazonS3Client(credentials, RegionEndpoint.USEast1);
        callGooglevision();
    }
    public void callGooglevision()
    {
        print("GV");
        print("filepath is " + filepath);
        Image<Bgr, Byte> googlevisionpicture = new Image<Bgr, byte>(filepath);
        imagebytes = googlevisionpicture.Bytes;
        PostObject("testpicture12.jpg"); 
        StartCoroutine("OnPostRender");
        //PostObject("testpicture12.jpg");
    }
    
    public void PostObject(string fileName)
    {
        print(fileName);
        string alternatefileName = "testpicture12.jpg";
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;
        var stream = new FileStream(filepath,
        FileMode.Open, FileAccess.Read, FileShare.Read);

        var request = new PutObjectRequest()
        {
            BucketName = "visionprocessing",
            Key = fileName,
            InputStream = stream,
            CannedACL = S3CannedACL.Private,
            //Region = _S3Region,
            ContentType = "image/jpg"
        };

        print(request);
        print(Client);
        Client.PutObjectAsync(request, (responseObj) =>
        {
            if (responseObj.Exception == null)
            {
                //ResultText.text += string.Format("\nobject {0} posted to bucket {1}",
                //responseObj.Request.Key, responseObj.Request.Bucket);
                print("no error");
            }
            else
            {
                //ResultText.text += "\nException while posting the result object";
                //ResultText.text += string.Format("\n receieved error {0}",
                //responseObj.Response.HttpStatusCode.ToString());
                print(responseObj.Exception.ToString());
            }
        });

    }
    
    public IEnumerator OnPostRender()
    {
        
        print("aws");
        Hashtable postHeader = new Hashtable();
        postHeader.Add("Content-Type", "image/png");

        var formData = System.Text.Encoding.UTF8.GetBytes(imagebytes.ToString());
        WWWForm form = new WWWForm();
        form.AddBinaryData("byte array", imagebytes);
        //WWW www = new WWW(apilink, imagebytes, postHeader);
        WWW www = new WWW(apilink);
        print("HERE");
        yield return www;
        res = www.text;
        print("HERE");
        res = www.text;
        print("gv res is "+res);
    
        GetObject2();


        //HOGImage hogimage = new HOGImage();
        //hogimage = JsonUtility.FromJson<HOGImage>(res);
        //vector = hogimage.hogoutput;
        res = www.text;
    }
    
    public void GetObject2()
    {
        //ResultText.text = string.Format("fetching {0} from bucket {1}", SampleFileName, S3BucketName);
        print("getObject getObject getObject");
        string BucketName = "visionprocessing";
        string FileName = "vission_links1.txt";
        //string FileName = "image_data1.txt";
        Client.GetObjectAsync(BucketName, FileName, (responseObj) =>
        {
            print("about to get json");

            if (responseObj.Exception == null) print("exception not null"); else print(responseObj.Exception.Message);
            string data = null;
            var resp = responseObj.Response;
            if (resp == null)
            {
                print("null");
            }
            else
            {
                print("not null");
            }
            print(resp.ContentLength);
            if (resp.ResponseStream != null)
            {
                print("before StreamReader");
                using (StreamReader reader = new StreamReader(resp.ResponseStream))
                {
                    print("In StreamReader");
                    data = reader.ReadToEnd();
                    print("got json");
                }

            }
            else
            {
                print("response is null");
            }
            print("JSON CONTENT: " + data);
            string[] gvlinksarr = data.Split('"');

            print(gvlinksarr[1]);
            print(gvlinksarr[2]);
            string gvlink1 = parseLink(gvlinksarr[1]);
            string gvlink2 = parseLink(gvlinksarr[2]);
            link1.GetComponent<Text>().text = gvlink1;
            link2.GetComponent<Text>().text = gvlink2;
            //gvlinks.GetComponent<Text>().text = gvlinksarr[1];
            //float[] fres = parseVectors(data);
            //for (int i = 0; i < fres.Length; i++) print(fres[i] + " ");

        });
    }
    
    public string parseLink(string s)
    {
        string res = "";
        int i = 0;
        char[] c = s.ToCharArray();
        while (i < s.Length && c[i] != 'u')
        {
            res += c[i] + "";
            i = i + 1;
        }
        return res;
        
    }
    // Update is called once per frame
    void Update () {
		
	}
}
