﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LinkCall : MonoBehaviour {
    public GameObject linkbutton;
    private string linkname;

    // Use this for initialization
    void Start () {
        //print("link button started");
        linkname = linkbutton.GetComponent<Text>().text;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void whenClicked()
    {
        print("link button pressed");
        linkname = linkbutton.GetComponent<Text>().text;
        print(linkname);
        Application.OpenURL(linkname);
    }
    public IEnumerator callLink(string linkname)
    {
        WWW linkcall = new WWW(linkname);
        yield return linkcall;
        print(linkcall.text);
    }
}
