﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Lambda;
using Amazon.S3;
using UnityEngine.Networking;
using Amazon.Lambda.Model;
using System.Text;
using UnityEngine.SceneManagement;
using Amazon.S3.Model;
using System.IO;

public class loginpage : MonoBehaviour
{
    CognitoAWSCredentials credentials;
    string apiLink = "https://bkzhjn1yob.execute-api.us-east-1.amazonaws.com/prod/loginpage";
    public InputField username;
    public InputField password;
    
    // Start is called before the first frame update
    void Start()
    {
        UnityInitializer.AttachToGameObject(this.gameObject);
        credentials = new CognitoAWSCredentials(
   "us-east-1:cda77550-d624-4016-ac43-5887e4a8bdf1", // Identity pool ID
   RegionEndpoint.USEast1 // Region
);

    }

    public IEnumerator SendLoginStuff()
    {
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;
        AmazonLambdaClient client = new AmazonLambdaClient(credentials, RegionEndpoint.USEast1);

        WWWForm userdata = new WWWForm();
        userdata.AddField("username",username.text);
        userdata.AddField("password",password.text);

        WWW send = new WWW(apiLink,userdata);
        yield return send;
        Debug.Log(send.text);


    }

    public void runEnumerator()
    {
        StartCoroutine(SendLoginStuff());
    }



}
